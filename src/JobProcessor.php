<?php

namespace L2T\PubSub;

use Illuminate\Queue\Jobs\Job;
use Google\Cloud\PubSub\Message;
use Illuminate\Container\Container;
use Illuminate\Contracts\Queue\Job as JobContract;

class JobProcessor extends Job implements JobContract
{
    /**
     * The job instance.
     *
     * @var Message
     */
    protected Message $job;

    /**
     * @var array
     */
    private array $decoded;

    /**
     * The PubSub queue.
     *
     * @var QueueProcessor
     */
    protected QueueProcessor $pubSub;

    /**
     * Create a new job instance.
     *
     * @param Container $container
     * @param QueueProcessor $pubSub
     * @param Message $job
     * @param string $connectionName
     * @param string $queue
     */
    public function __construct(
        Container      $container,
        QueueProcessor $pubSub,
        Message        $job,
        string         $connectionName,
        string         $queue
    ) {
        $this->job = $job;
        $this->queue = $queue;
        $this->pubSub = $pubSub;
        $this->container = $container;
        $this->connectionName = $connectionName;

        $this->decoded = $this->payload();
    }

    /**
     * Get the job identifier.
     *
     * @return string|null
     */
    public function getJobId(): ?string
    {
        return $this->decoded['id'] ?? null;
    }

    /**
     * Get the raw body of the job.
     *
     * @return string
     */
    public function getRawBody(): string
    {
        return base64_decode($this->job->data());
    }

    /**
     * Get the number of times the job has been attempted.
     *
     * @return int
     */
    public function attempts(): int
    {
        return ((int) $this->job->attribute('attempts') ?? 0) + 1;
    }

    /**
     * Release the job back into the queue.
     *
     * @param  int  $delay
     * @return void
     */
    public function release($delay = 0)
    {
        parent::release($delay);

        $attempts = $this->attempts();
        $this->pubSub->republish(
            $this->job,
            $this->queue,
            ['attempts' => (string) $attempts],
            $delay
        );
    }
}
