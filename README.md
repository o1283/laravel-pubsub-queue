### Laravel Pub/Sub Queue

If you want to get support of pub/sub queue driver in laravel this package is ideal for your needs.

Follow installation below or on my blog: https://learn2torials.com/a/laravel9-pub-sub-integration


### How to install this package
```bash
# install composer dependency
composer require l2t/laravel-pubsub-queue


# go to your laravel config/queue.php file and add following block
'connections' => [
    
    // other settings
    
    'pubsub' => [
        'retries' => 3,
        'driver' => 'pubsub',
        'request_timeout' => 60,
        'queue' => env('PUBSUB_DEFAULT_QUEUE'),
        'project_id' => env('GOOGLE_CLOUD_PROJECT'),
        'queue_prefix' => env('PUBSUB_QUEUE_PREFIX', ''),
        'keyFilePath' => storage_path(env('GOOGLE_APPLICATION_CREDENTIALS')),
    ],    
]

# let's create a directory in storage folder
mkdir -p storage/secrets

# add following new env variables in .env file
# google credentials
PUBSUB_DEFAULT_QUEUE=<topic-name>
GOOGLE_CLOUD_PROJECT=<project-name>
GOOGLE_APPLICATION_CREDENTIALS=secrets/<google-credentials>.json

# enable pub/sub queue driver in .env
QUEUE_CONNECTION=pubsub
```

### How to dispatch job to pub/sub queue
```php
# queue jobs to default topic
dispatch(new PubSubJob());

# queue jobs to specific topic
dispatch(new PubSubJob())->onQueue(env('PUBSUB_CUSTOM_QUEUE'));

# process the topic in pubsub
php artisan queue:work pubsub

# process the topic 
php artisan queue:work --queue=<some-topic>
```

That is it now you should be able to use pub/sub queues in your laravel project.
